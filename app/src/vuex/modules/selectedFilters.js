import * as types from '../mutation-types'

const state = {
  function: "",
  cluster: "",
  department:"",
  service:"",
  team:""
}

const mutations = {
  [types.CHANGE_FUNCTION_FILTER] (state, id ) {
    if( id === state.function)
    {
        state.function = "";
    }else
    {
        state.function = id
    }

  },
  [types.CHANGE_CLUSTER_FILTER] (state, id ) {
    if( id === state.cluster)
    {
        state.cluster = "";
    }else
    {
        state.cluster = id
    }
    state.department = ''
    state.service = ''
    state.team = ''
  },
  [types.CHANGE_DEPARTMENT_FILTER] (state, id ) {
    if( id === state.department)
    {
        state.department = "";
    }else
    {
        state.department = id
    }
    state.service = ''
    state.team = ''
  },
  [types.CHANGE_SERVICE_FILTER] (state, id ) {
    if( id === state.service)
    {
        state.service = "";
    }else
    {
        state.service = id
    }
    state.team = ''
  },
  [types.CHANGE_TEAM_FILTER] (state, id ) {
    if( id === state.team)
    {
        state.team = "";
    }else
    {
        state.team = id
    }
  },
}

export default {
  state,
  mutations
}
