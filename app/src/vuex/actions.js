import * as types from './mutation-types'

export const decrementMain = ({ commit }) => {
  commit(types.DECREMENT_MAIN_COUNTER)
}

export const incrementMain = ({ commit }) => {
  commit(types.INCREMENT_MAIN_COUNTER)
}


export const changeFunctionFilter = ({ commit }, id) => {
  commit(types.CHANGE_FUNCTION_FILTER, id)
}
export const changeClusterFilter = ({ commit }, id) => {
  commit(types.CHANGE_CLUSTER_FILTER, id)
}
export const changeDepartmentFilter = ({ commit }, id) => {
  commit(types.CHANGE_DEPARTMENT_FILTER, id)
}
export const changeServiceFilter = ({ commit }, id) => {
  commit(types.CHANGE_SERVICE_FILTER, id)
}
export const changeTeamFilter = ({ commit }, id) => {
  commit(types.CHANGE_TEAM_FILTER, id)
}
