export const mainCounter = state => state.counters.main

export const functionFilter = state => state.selectedFilters.function
export const clusterFilter = state => state.selectedFilters.cluster
export const departmentFilter = state => state.selectedFilters.department
export const serviceFilter = state => state.selectedFilters.service
export const teamFilter = state => state.selectedFilters.team
