import elasticlunr from 'elasticlunr'


export function filterByTitle (value, title) {
  return filterBookmarks(value, 'title', title)
}

export function filterByCategory (value, category) {
  if (!category) return value
  return filterBookmarks(value, 'category', category)
}

function filterBookmarks (bookmarks, filterBy, filterValue) {
  var filteredBookmarks = {}
  for (var bookmark in bookmarks) {
    if (bookmarks[bookmark][filterBy].indexOf(filterValue) > -1) {
      filteredBookmarks[bookmark] = bookmarks[bookmark]
    }
  }
  return filteredBookmarks
}


export function filterEmployeeByName (employees, name) {
  // if name is undefined -> sort bij firstname lastname

  if(name == '' || name == undefined)
  {
    //console.log(employees)
    if(employees == undefined)
    {
      return employees
    }


    return employees.slice(0).sort(function(a, b) {
      if( a.firstname == undefined || b.firstname == undefined || a.lastname == undefined || b.lastname == undefined)
      {
        return 0
      }
      var nameA=a.firstname.toLowerCase() + ' ' + a.lastname.toLowerCase() , nameB=b.firstname.toLowerCase() + ' ' + b.lastname.toLowerCase()
      if (nameA < nameB) //sort string ascending
        return -1
      if (nameA > nameB)
        return 1
      return 0 //default return value (no sorting)
    })
  }
  var filteredEmployees = []
  var keyValueIndex = []
  var index = elasticlunr(function () {
   // this.use(lnrdulang)
    this.addField('firstname')
    this.addField('lastname')
    this.addField('description')
    this.setRef('id')
  })


  // if not do search with elasticlunr + keep that order. /
  for(let i= 0; i< employees.length; i++ )
  {
    let doc = {
      'id': employees[i]['.key'],
      'firstname': employees[i]['firstname'],
      'lastname': employees[i]['lastname'],
      'description': employees[i]['description'],
    }
    index.addDoc(doc)

    keyValueIndex[employees[i]['.key']] = employees[i]
  }

  var searchResult = index.search(name, {fields:{firstname: {boost: 2},lastname: {boost: 2},description: {boost: 1}},bool: 'OR',expand: true})
  //console.log(searchResult)
  for(let i = 0 ; i < searchResult.length; i++)
  {
    let result = keyValueIndex[searchResult[i].ref]
    //result.score = searchResult[i].score
    filteredEmployees.push(result)
  }
  return filteredEmployees
}

// filter
/*export function filterEmployeeByName (employees, name) {

  var filteredEmployees = {}
  for (var employee in employees) {
    if(employees[employee]['firstname'] && employees[employee]['lastname'] )
    {
      if (employees[employee]['firstname'].toUpperCase().indexOf(name.toUpperCase()) > -1 || employees[employee]['lastname'].toUpperCase().indexOf(name.toUpperCase()) > -1 ) {
        filteredEmployees[employee] = employees[employee]
      }
    }
  }
  return filteredEmployees

}*/

export function filterEmployeeByCluster (employees, name) {
  return filterEmployees(employees, 'cluster', name)
}

export function filterEmployeeByDepartment (employees, name) {
  return filterEmployees(employees, 'department', name)
}
export function filterEmployeeByService (employees, name) {
  return filterEmployees(employees, 'service', name)
}
export function filterEmployeeByTeam (employees, name) {
  return filterEmployees(employees, 'team', name)
}


export function filterByFunction (employees, functionId) {
  if (!functionId) return employees
  return filterEmployees (employees, 'function', functionId)
}


function filterEmployees (employees, filterBy, filterValue) {

  if(filterValue === '')
  {
    return employees
  }
  var filteredEmployees = []
  for (var employee in employees) {

    if(employees[employee][filterBy])
    {
      if (employees[employee][filterBy].toUpperCase().indexOf(filterValue.toUpperCase()) > -1) {
        filteredEmployees.push(employees[employee])
      }
    }
  }
  return filteredEmployees
}



function filterOrganisationalUnit (ous, filterBy, filterValue) {

  var filteredOu = {}
  for (var ou in ous) {
    if(ous[ou][filterBy])
    {
      if (ous[ou][filterBy] == filterValue) {
        filteredOu[ou] = ous[ou]
      }
    }
  }
  return filteredOu
}

export function filterDepartmentsByCluster (departments, clusterId) {
  //console.log(clusterId)
  if (!clusterId) return []
  return filterOrganisationalUnit (departments, 'cluster', clusterId)
}

export function filterServicesByDepartment (services, departmentId) {
  if (!departmentId) return []
  return filterOrganisationalUnit (services, 'department', departmentId)
}

export function filterTeamsByService (teams, serviceId) {
  if (!serviceId) return []
  return filterOrganisationalUnit (teams, 'service', serviceId)
}
