export default [
  {
    path: '/',
    name: 'landing-page',
    component: require('components/SearchView.vue')
  },
  {
    path: '/login',
    component: require('components/LoginView.vue'),
    name: 'login'
  },
  {
    path: '/settings',
    component: require('components/SettingsView.vue'),
    name: 'settings'
  },
  {
    path: '/employee',
    component: require('components/EmployeeView.vue'),
    name: 'newemployee'
  },
  {
    path: '/employee/:employeeid',
    component: require('components/EmployeeView.vue'),
    name: 'employee'
  },
  {
    path: '/hierarchie/:employeeid/:bossid',
    component: require('components/EmployeeHierarchie.vue'),
    name: 'hierarchie'
  },
  {
    path: '/functions',
    component: require('components/FunctionsView.vue'),
    name: 'functions'
  },
 {
    path: '/clusters',
    component: require('components/ClustersView.vue'),
    name: 'clusters'
  },
  {
    path: '/departments/:clusterId',
    component: require('components/DepartmentsView.vue'),
    name: 'servicesbycluster'
  },
  {
    path: '/services/:departmentId',
    component: require('components/ServicesView.vue'),
    name: 'servicesbydepartment'
  },
  {
    path: '/teams/:serviceId',
    component: require('components/TeamsView.vue'),
    name: 'teamsbyservice'
  },
  {
    path: '*',
    component: require('components/NotFound.vue'),
    name: 'notFound'
  }
]
