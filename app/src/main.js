import Vue from 'vue'
import Electron from 'vue-electron'
import Router from 'vue-router'
import VueFire from 'vuefire'
import VeeValidate from 'vee-validate';
import VeeValidateLocaleNL from './strings/validator/messages/nl.js';
import VeeValidateLocaleEN from './strings/validator/messages/en.js';
import VeeValidateLocaleFR from './strings/validator/messages/fr.js';

import App from './App'
import routes from './routes'

Vue.use(Electron)
Vue.use(Router)
Vue.use(VueFire)

const config = {
    errorBagName: 'errors', // change if property conflicts.
    fieldsBagName: 'fields',
    delay: 0,
    locale: 'nl',
    dictionary: { // dictionary object
        en: {  // locale key
            messages:VeeValidateLocaleEN,
            "employee.firstname": "First Name" // English attributes
        },
        nl: {   // locale key
            messages:VeeValidateLocaleNL,
            attributes:{
                "employee.firstname": "Voornaam",   // Dutch messages
                "employee.lastname": "Familienaam",   // Dutch messages
                "employee.email": "Email",   // Dutch messages
                "employee.cell": "GSM",   // Dutch messages
                "employee.phone": "Telefoon",   // Dutch messages
            }
        },
        fr: {   // locale key
            messages:VeeValidateLocaleFR,
            attributes:{
                "employee.firstname": "Prénom",   // French messages
                "employee.lastname": "nom",   // French messages
            }
        }
    },
    strict: true,
    enableAutoClasses: false,
    classNames: {
        touched: 'touched', // the control has been blurred
        untouched: 'untouched', // the control hasn't been blurred
        valid: 'valid', // model is valid
        invalid: 'invalid', // model is invalid
        pristine: 'pristine', // control has not been interacted with
        dirty: 'dirty' // control has been interacted with
    }
};

Vue.use(VeeValidate, config);

Vue.config.debug = true

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes
})

/* eslint-disable no-new */
new Vue({
  router,
  ...App
}).$mount('#app')
