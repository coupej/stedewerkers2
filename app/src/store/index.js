import { EventEmitter } from 'events'

var firebase = require('firebase/app');
// all 3 are optional and you only need to require them at the start
require('firebase/auth');
require('firebase/database');
require('firebase/storage');
import errorMessages from '../config/errorMessages.json'
import connectionConfig from '../config/connection.json'

//import elasticlunr from 'elasticlunr'


firebase.initializeApp(connectionConfig)

//var eventHub = new Vue()

const db = firebase.database().ref()
const storage = firebase.storage()
const storageRef = storage.ref()

const employeesRef = db.child('employees')
const functionsRef = db.child('functions')
const clustersRef = db.child('clusters')
const departmentsRef = db.child('departments')
const servicesRef = db.child('services')
const teamsRef = db.child('teams')
const store = new EventEmitter()

store.AuthChanged=(callback)=>{
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
      console.log('user signed in' +user.uid)
      //loggedInUser = db.child('users').child(user.uid)

      localStorage.setItem('loggedInUserUid', user.uid)

      callback(user)
    } else {
      // No user is signed in.
      console.log('nope')
      callback()
    }
  })
}


store.logout = (callback)=>
{
  firebase.auth().signOut().then(function() {
    // Sign-out successful.
    console.log('succesvol uitgelogd')
    localStorage.setItem('loggedInUser', '')
    localStorage.setItem('loggedInUserUid', '')
    callback()
  }, function(error) {
    // An error happened.
    console.log('succesvol uitgelogd')
    callback(error)
  })
}


store.logIn =( username , password, callback )=>
{

  if( username != undefined && password != undefined && username != '' && password != ''  )
  {
    let result = firebase.auth().signInWithEmailAndPassword(username, password).catch(function(error) {
  // Handle Errors here.
      var errorCode = error.code
      var errorMessage = error.message

      errorMessage = errorMessages[errorCode]


      callback(errorMessage)
      return true
    })
    console.log(result)

  }else
  {
    callback('Gelieve paswoord en gebruikersnaam in te geven.')
    return false
  }
  //firebase.auth().signInWithEmailAndPassword('jeroencoupe@gmail.com', 'Test123')

}

/*functions CRUD*/
store.addFunction = (functionObject) => {
  var saveObject = store.prepareSaveObject(functionObject)
  functionsRef.push().set(saveObject)
}

store.updateFunction = (functionObject) => {
  var saveObject = store.prepareSaveObject(functionObject)
  functionsRef.child(functionObject['.key']).update(saveObject)
}

store.deleteFunction = (functionObject) => {
  functionsRef.child(functionObject['.key']).remove()
}
/*end functions CRUD*/

/*cluster CRUD*/
store.addCluster = (clusterObject) => {
  var saveObject = store.prepareSaveObject(clusterObject)
  clustersRef.push().set(saveObject)
}

store.updateCluster = (clusterObject) => {
  var saveObject = store.prepareSaveObject(clusterObject)
  clustersRef.child(clusterObject['.key']).update(saveObject)
}

store.deleteCluster = (clusterObject) => {
  clustersRef.child(clusterObject['.key']).remove()
}
/*end clusters CRUD*/

/*departments CRUD*/
store.addDepartment = (departmentObject) => {
  var saveObject = store.prepareSaveObject(departmentObject)
  departmentsRef.push().set(saveObject)
}

store.updateDepartment = (departmentObject) => {
  var saveObject = store.prepareSaveObject(departmentObject)
  departmentsRef.child(departmentObject['.key']).update(saveObject)
}

store.deleteDepartment = (departmentObject) => {
  departmentsRef.child(departmentObject['.key']).remove()
}
/*end departments CRUD*/

/* services crud*/
store.addService = (serviceObject) => {
  var saveObject = store.prepareSaveObject(serviceObject)
  servicesRef.push().set(saveObject)
}

store.updateService = (serviceObject) => {
  var saveObject = store.prepareSaveObject(serviceObject)
  servicesRef.child(serviceObject['.key']).update(saveObject)
}

store.deleteService = (serviceObject) => {
  servicesRef.child(serviceObject['.key']).remove()
}
/*end services CRUD*/


/* teams crud*/
store.addTeam = (teamObject) => {
  var saveObject = store.prepareSaveObject(teamObject)
  teamsRef.push().set(saveObject)
}

store.updateTeam = (teamObject) => {
  var saveObject = store.prepareSaveObject(teamObject)
  teamsRef.child(teamObject['.key']).update(saveObject)
}

store.deleteTeam = (teamObject) => {
  teamsRef.child(teamObject['.key']).remove()
}
/*end teams CRUD*/


store.addEmployee = (employee) => {
  employeesRef.push(employee)
}

store.deleteEmployee = (employee) => {
  employeesRef.child(employee['.key']).remove()
}

store.updateEmployee = (employee) => {
  console.log('updating...')
  var saveObject = store.prepareSaveObject(employee)
  employeesRef.child(employee['.key']).update(saveObject)
}

store.createEmployee = (employee) => {
  var saveObject = store.prepareSaveObject(employee)
  employeesRef.push().set(saveObject)
}


store.prepareSaveObject= (employee) =>
{
  var saveObject = {}
  for(var key in employee) {
    if( employee[key] !== undefined && key !== '.key')
      saveObject[key]=employee[key]
      //var value = objects[key];
  }
  console.log(saveObject)
  return saveObject
}

store.db = (name) => {
  return db.child(name)
}

store.storage = (name) => {
  //return name;
  return storageRef.child(name)
}

export default store
